//Tarea | Mongodb 101
//1) Baja el archivo grades.json y en la terminal ejecuta el siguiente comando:
//$ mongoimport -d students -c grades < grades.json

// 2) Confirma que se importó correctamente la colección:
use students;
db.grades.count();
// Respuesta: El comando "count" devuelve el número de registros en la colección: "800".

// 3) Encuentra todas las calificaciones del estudiante con el id número 4.
db.grades.find({ "student_id": 4 });

 /*
Respuesta:
{ "_id" : ObjectId("50906d7fa3c412bb040eb588"), "student_id" : 4, "type" : "quiz", "score" : 27.29006335059361 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb587"), "student_id" : 4, "type" : "exam", "score" : 87.89071881934647 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb589"), "student_id" : 4, "type" : "homework", "score" : 5.244452510818443 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb58a"), "student_id" : 4, "type" : "homework", "score" : 28.656451042441 }
 */

// 4) ¿Cuántos registros hay de tipo "exam"?
db.grades.find({ "type": "exam" }).count();
//Respuesta: "200"

// 5) ¿Cuántos registros hay de tipo "homework"?
db.grades.find({ "type": "homework" }).count();
//Respuesta: "400"

// 6) ¿Cuántos registros hay de tipo "quiz"?
db.grades.find({ "type": "quiz" }).count();
//Respuesta: "200"

// 7) Elimina todas las calificaciones del estudiante con el id número 3.
db.grades.remove({ "student_id": 3 });
//La respuesta mostrada es: WriteResult({ "nRemoved" : 4 })

// 8) ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea?
db.grades.find({ "score": 75.29561445722392 });
//La respuesta encontrada es: { "_id" : ObjectId("50906d7fa3c412bb040eb59e"), "student_id" : 9, "type" : "homework", "score" : 75.29561445722392 }

// 9) Actualiza las calificaciones del registro con el UUID "50906d7fa3c412bb040eb591" a 100.
db.grades.updateOne( { "_id": ObjectId("50906d7fa3c412bb040eb591") }, { $set: { "score": 100 } } );

//La respuesta mostrada es: { "acknowledged" : true, "matchedCount" : 1, "modifiedCount" : 1 }

// 10) A qué estudiante pertenece esta calificación.
db.grades.findOne({ "score": 100 });

/* Respuesta:
{
        "_id" : ObjectId("50906d7fa3c412bb040eb591"),
        "student_id" : 6,
        "type" : "homework",
        "score" : 100
}
    */